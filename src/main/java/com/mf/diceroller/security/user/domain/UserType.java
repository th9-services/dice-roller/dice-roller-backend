package com.mf.diceroller.security.user.domain;

import org.springframework.security.core.GrantedAuthority;

public enum UserType implements GrantedAuthority {
    ORDINARY_PLAYER("ORDINARY_PLAYER"),
    GAME_MASTER("GAME_MASTER"),
    SUPER_USER("SUPER_USER");

    private String authority;
    private UserType(final String authority) {
        this.authority = authority;
    }

    @Override
    public String getAuthority() {
        return this.authority;
    }
}
