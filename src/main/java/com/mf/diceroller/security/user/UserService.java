package com.mf.diceroller.security.user;

import com.mf.diceroller.security.user.domain.CustomUserDetails;
import com.mf.diceroller.security.user.domain.User;
import com.mf.diceroller.security.user.domain.UserCreator;
import com.mf.diceroller.security.user.domain.UserType;
import com.mf.diceroller.security.user.entity.UserEntity;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
@RequiredArgsConstructor
public class UserService {
    private final UserRepository userRepository;


    public void clearRoom(final String roomId) {
        this.userRepository.deleteByRoomId(roomId);
    }

    public void deletePlayer(final User player) {
        userRepository.deleteByUsername(player.getUsername());
    }

    public User createPlayer(final CustomUserDetails userDetails) {
        return UserCreator.from(userDetails);
    }

    public User generateGameMaster(final String roomId, final String nickname) {
        return this.generateUser(roomId, nickname, UserType.GAME_MASTER);
    }

    public User generatePlayer(final String roomId, final String nickname) {
        return this.generateUser(roomId, nickname, UserType.ORDINARY_PLAYER);
    }

    private User generateUser(final String roomId, final String nickname, final UserType userType) {
        final UserEntity userEntity = new UserEntity(
                UUID.randomUUID().toString(),
                "{noop}" + UUID.randomUUID().toString(),
                roomId,
                nickname,
                userType
        );
        this.userRepository.save(userEntity);

        return UserCreator.from(new CustomUserDetails(userEntity));
    }
}

