package com.mf.diceroller.security.user.dto;

import com.mf.diceroller.security.user.domain.UserType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Getter
public class UserCredentialsDTO {
    private String username;
    private String password;
    private UserType userType;
}
