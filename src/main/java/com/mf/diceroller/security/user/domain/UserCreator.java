package com.mf.diceroller.security.user.domain;


import com.mf.diceroller.security.user.dto.UserDTO;

import static java.util.Objects.requireNonNull;

public class UserCreator {
    public static User from(final UserDTO dto, final String roomId, final UserType userType) {
        requireNonNull(dto);
        requireNonNull(roomId);


        return User.builder()
                .username(dto.getUsername())
                .nickname(dto.getNickname())
                .roomId(roomId)
                .userType(userType)
                .build();
    }

    public static User from(final CustomUserDetails userDetails) {
        requireNonNull(userDetails);

        return User.builder()
                .username(userDetails.getUsername())
                .password(userDetails.getPassword().substring("{noop}".length()))
                .roomId(userDetails.getRoomId())
                .nickname(userDetails.getNickname())
                .userType(userDetails.getUserType())
                .build();
    }
}
