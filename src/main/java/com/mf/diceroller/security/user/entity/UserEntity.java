package com.mf.diceroller.security.user.entity;

import com.mf.diceroller.security.user.domain.UserType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class UserEntity {
    @Id
    @Column(nullable = false, unique = true)
    private String username;
    private String password;
    private String roomId;
    private String nickname;
    private UserType userType;
}

