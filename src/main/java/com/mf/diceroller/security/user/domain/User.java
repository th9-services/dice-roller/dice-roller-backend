package com.mf.diceroller.security.user.domain;


import com.mf.diceroller.round.model.domain.DiceRoll;
import com.mf.diceroller.round.model.domain.DiceRollResults;
import com.mf.diceroller.round.model.domain.DiceRollSetup;
import com.mf.diceroller.security.user.dto.UserCredentialsDTO;
import com.mf.diceroller.security.user.dto.UserDTO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;
import java.util.stream.Collectors;

@AllArgsConstructor
@Data
@Builder
@EqualsAndHashCode
public class User {
    private String username;
    private String password;
    private String roomId;
    private String nickname;
    private UserType userType;

    public UserDTO asDTO() {
        return new UserDTO(
                this.username,
                this.nickname
        );
    }

    public UserCredentialsDTO asUserCredentialsDTO() {
        return new UserCredentialsDTO(
                this.username,
                this.password,
                this.userType
        );
    }

    public boolean isInRoom(final String roomId) {
        return this.getRoomId().equals(roomId);
    }

    public DiceRollResults rollDices(final DiceRollSetup setup) {
        final List<DiceRoll> results = setup.getDices()
                .stream()
                .map(dice -> setup.getStrategy().rollDice(dice))
                .collect(Collectors.toList());

        return new DiceRollResults(results);
    }

}
