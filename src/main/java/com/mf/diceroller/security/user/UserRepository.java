package com.mf.diceroller.security.user;

import com.mf.diceroller.security.user.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

public interface UserRepository extends JpaRepository<UserEntity, Long> {

    @Modifying
    @Transactional
    @Query("delete from UserEntity u where u.roomId in ?1")
    void deleteByRoomId(final String roomId);

    @Modifying
    @Transactional
    @Query("delete from UserEntity u where u.username = ?1")
    void deleteByUsername(final String username);

    UserEntity findByUsername(final String username);
}
