package com.mf.diceroller.round.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class DiceRollDTO {
    private long max;
    private long result;
}
