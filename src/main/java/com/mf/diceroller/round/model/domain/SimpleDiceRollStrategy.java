package com.mf.diceroller.round.model.domain;

public class SimpleDiceRollStrategy implements DiceRollStrategy {

    @Override
    public DiceRoll rollDice(long dice) {
        return new DiceRoll(
                dice,
                getRandomLongInRange(1l, dice)
        );
    }

    private long getRandomLongInRange(final long from, final long to) {
        return from +  Math.round(Math.random() * (to - from));
    }
}
