package com.mf.diceroller.round.model.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
public class GameLogDTO {
    List<GameLogEntryDTO> entries;
}
