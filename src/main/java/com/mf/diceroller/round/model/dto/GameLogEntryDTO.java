package com.mf.diceroller.round.model.dto;

import com.mf.diceroller.security.user.dto.UserDTO;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;


@AllArgsConstructor
@NoArgsConstructor
@Getter
public class GameLogEntryDTO {
    private Long time;
    private DiceRollsResultsDTO rolls;
    private UserDTO player;

}
