package com.mf.diceroller.round.model.domain;

import com.mf.diceroller.round.model.dto.GameRulesDTO;
import com.mf.diceroller.security.user.domain.User;
import com.mf.diceroller.security.user.dto.UserDTO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.util.List;
import java.util.stream.Collectors;

@AllArgsConstructor
@Data
@Builder
public class GameRules {
    private List<Long> dices;
    private List<User> players;

    public GameRulesDTO asDTO() {
        final List<UserDTO> playerDTOS = this.getPlayers()
                .stream()
                .map(User::asDTO)
                .collect(Collectors.toList());

        return new GameRulesDTO(
                this.getDices(),
                playerDTOS
        );
    }
}
