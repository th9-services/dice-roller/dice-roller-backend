package com.mf.diceroller.round.model.domain;

import com.mf.diceroller.round.model.dto.GameLogEntryDTO;
import com.mf.diceroller.security.user.domain.User;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.Instant;

@AllArgsConstructor
@Data
public class GameLogEntry {
    private Instant time;
    private DiceRollResults rolls;
    private User player;

    public GameLogEntryDTO asDTO() {
        return new GameLogEntryDTO(
                time.toEpochMilli(),
                rolls.asDTO(),
                player.asDTO()
        );
    }
}
