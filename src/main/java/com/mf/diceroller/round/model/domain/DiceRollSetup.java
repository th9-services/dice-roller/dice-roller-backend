package com.mf.diceroller.round.model.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@AllArgsConstructor
@Data
@Builder
public class DiceRollSetup {
    private List<Long> dices;
    private DiceRollStrategy strategy;

}
