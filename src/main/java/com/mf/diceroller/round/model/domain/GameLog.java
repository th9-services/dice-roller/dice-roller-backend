package com.mf.diceroller.round.model.domain;

import com.mf.diceroller.round.model.dto.GameLogDTO;
import com.mf.diceroller.round.model.dto.GameLogEntryDTO;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Getter
public class GameLog {
    List<GameLogEntry> entries = new ArrayList<>();

    public GameLogDTO asDTO() {
        final List<GameLogEntryDTO> entryDTOS = entries.stream()
                .map(GameLogEntry::asDTO)
                .collect(Collectors.toList());

        return new GameLogDTO(entryDTOS);
    }

    public void addEntry(final GameLogEntry entry) {
        entries.add(entry);
    }
}
