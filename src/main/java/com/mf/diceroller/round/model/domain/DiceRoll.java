package com.mf.diceroller.round.model.domain;

import com.mf.diceroller.round.model.dto.DiceRollDTO;
import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class DiceRoll {
    private long max;
    private long result;

    public DiceRollDTO asDTO() {
        return new DiceRollDTO(this.max, this.result);
    }
}
