package com.mf.diceroller.round.model.domain;

import com.mf.diceroller.round.model.dto.DiceRollSetupDTO;

public class DiceRollSetupCreator {
    public static DiceRollSetup from(final DiceRollSetupDTO dto) {
        return DiceRollSetup.builder()
                .dices(dto.getDices())
                .strategy(new SimpleDiceRollStrategy())
                .build();
    }
}
