package com.mf.diceroller.round.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class DiceRollsResultsDTO {
    private List<DiceRollDTO> results;
}
