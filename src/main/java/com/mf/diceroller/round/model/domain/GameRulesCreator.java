package com.mf.diceroller.round.model.domain;

import com.mf.diceroller.round.model.dto.GameRulesDTO;
import com.mf.diceroller.security.user.domain.User;
import com.mf.diceroller.security.user.domain.UserCreator;
import com.mf.diceroller.security.user.domain.UserType;

import java.util.List;
import java.util.stream.Collectors;

import static java.util.Objects.requireNonNull;

public class GameRulesCreator {
    public static GameRules from(final GameRulesDTO dto, final String roomId) {
        requireNonNull(dto);

        final List<User> players = dto.getPlayers()
                .stream()
                .map(player -> UserCreator.from(player, roomId, UserType.ORDINARY_PLAYER))
                .collect(Collectors.toList());

        return GameRules.builder()
                .dices(dto.getDices())
                .players(players)
                .build();
    }
}
