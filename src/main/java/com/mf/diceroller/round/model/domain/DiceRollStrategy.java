package com.mf.diceroller.round.model.domain;

public interface DiceRollStrategy {
    DiceRoll rollDice(final long dice);
}
