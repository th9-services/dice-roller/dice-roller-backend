package com.mf.diceroller.round.model.dto;

import com.mf.diceroller.security.user.dto.UserDTO;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
public class GameRulesDTO {
    private List<Long> dices;
    private List<UserDTO> players;
}
