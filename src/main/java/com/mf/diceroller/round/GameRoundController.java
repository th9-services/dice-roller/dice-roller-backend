package com.mf.diceroller.round;

import com.mf.diceroller.room.service.GameRoomsService;
import com.mf.diceroller.round.model.dto.DiceRollSetupDTO;
import com.mf.diceroller.round.model.dto.DiceRollsResultsDTO;
import com.mf.diceroller.round.model.dto.GameRulesDTO;
import com.mf.diceroller.security.user.domain.CustomUserDetails;
import lombok.RequiredArgsConstructor;
import org.springframework.hateoas.EntityModel;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

@RequestMapping("api/round")
@RestController
@RequiredArgsConstructor
public class GameRoundController {
    private final GameRoomsService gameRoomsService;

    @PostMapping("/update-rules")
    @PreAuthorize("hasAnyAuthority('GAME_MASTER')")
    public ResponseEntity<?> updateRules(@RequestBody GameRulesDTO rules, @RequestParam String roomId) {
        this.gameRoomsService.updateRules(roomId, rules);
        return ResponseEntity
                .ok()
                .build();
    }

    @PostMapping("/make-roll")
    @PreAuthorize("hasAnyAuthority('GAME_MASTER', 'ORDINARY_PLAYER')")
    public EntityModel<DiceRollsResultsDTO> makeARoll(@RequestParam String roomId, @RequestBody DiceRollSetupDTO diceRollSetupDTO, Authentication authentication) {
        final DiceRollsResultsDTO results = this.gameRoomsService.makeARoll(diceRollSetupDTO, roomId, (CustomUserDetails) authentication.getPrincipal());
        return new EntityModel<>(results);
    }
}
