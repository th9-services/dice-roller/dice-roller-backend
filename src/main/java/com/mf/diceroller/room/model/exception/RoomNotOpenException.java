package com.mf.diceroller.room.model.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_ACCEPTABLE, reason = "ROOM_NOT_OPEN")
public class RoomNotOpenException extends RuntimeException {
}
