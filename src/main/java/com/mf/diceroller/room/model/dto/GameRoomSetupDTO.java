package com.mf.diceroller.room.model.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
public class GameRoomSetupDTO {
    private List<Long> dices;
    private GameRoomCredentialsDTO roomCredentials;
    private String playerNickname;
    private GameRoomConfigDTO config;
}
