package com.mf.diceroller.room.model.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class GameRoomCredentialsDTO {
    private String roomName;
    private String roomPassword;
}
