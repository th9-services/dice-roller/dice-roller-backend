package com.mf.diceroller.room.model.dto;

import com.mf.diceroller.security.user.dto.UserCredentialsDTO;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Getter
public class GameRoomUserCredentialsDTO {
    private String roomId;
    private UserCredentialsDTO credentials;
}
