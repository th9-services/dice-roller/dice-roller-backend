package com.mf.diceroller.room.model.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_ACCEPTABLE, reason = "ROOM_NO_ACCESS")
public class RoomNoAccessException extends RuntimeException {
}
