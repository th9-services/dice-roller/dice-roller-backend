package com.mf.diceroller.room.model.domain;

import com.mf.diceroller.round.model.domain.DiceRollResults;
import com.mf.diceroller.round.model.domain.GameRules;
import com.mf.diceroller.security.user.domain.User;
import lombok.AllArgsConstructor;
import lombok.Getter;
import reactor.core.publisher.Flux;
import reactor.core.publisher.FluxProcessor;
import reactor.core.publisher.FluxSink;

import java.time.Instant;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@AllArgsConstructor
public class GameRoomExtended {
    private FluxProcessor<GameRoom, GameRoom> processor;
    private FluxSink sink;
    @Getter
    private GameRoom lastValue;
    @Getter
    private Date lastActivity;

    public GameRoomExtended(FluxProcessor<GameRoom, GameRoom> processor, FluxSink sink, GameRoom lastValue) {
        this.processor = processor;
        this.sink = sink;
        this.lastValue = lastValue;
    }

    public void complete() {
        this.sink.complete();
    }

    public void update() {
        lastActivity = new Date();
        this.sink.next(this.lastValue);
    }

    public Flux<GameRoom> getStream() {
        return this.processor.map(room -> room);
    }

    public void closeRoom() {
        this.lastValue.closeRoom();
        this.update();
    }

    public void openRoom() {
        this.lastValue.openRoom();
        this.update();
    }

    public void updateRules(final GameRules rules) {
        this.lastValue.setRules(rules);
        this.update();
    }

    public void updateDices(final List<Long> dices) {
        lastValue.setDices(dices);

        if (lastValue.getConfig().isAutoEnableDices()) {
            lastValue.getRules().setDices(dices);
        } else {
            final List<Long> ruleDices = lastValue.getRules().getDices().stream()
                    .filter(dices::contains)
                    .collect(Collectors.toList());
            lastValue.getRules().setDices(ruleDices);
        }

        update();
    }

    public void updateConfig(final GameRoomConfig config) {
        lastValue.setConfig(config);
        update();
    }

    public void addRollResults(final User player, final DiceRollResults diceRollResults, final Instant time) {
        lastValue.addRollResults(player, diceRollResults, time);
        this.update();
    }


    public void addPlayer(final User player) {
        this.lastValue.getPlayers()
                .add(player);

        if (lastValue.getConfig().isAutoEnablePlayers()) {
            lastValue.getRules().getPlayers()
                    .add(player);
        }

        this.update();
    }

    public Optional<User> getPlayerByUsername(final String username) {
        return this.lastValue.getPlayers().stream()
                .filter(player -> player.getUsername().equals(username))
                .findFirst();
    }

    public void kickPlayer(final User player) {
        this.lastValue.getPlayers().remove(player);
        this.lastValue.getRules().getPlayers().remove(player);
        this.update();
    }
}
