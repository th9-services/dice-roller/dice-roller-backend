package com.mf.diceroller.room.model.dto;

import com.mf.diceroller.round.model.dto.GameLogDTO;
import com.mf.diceroller.round.model.dto.GameRulesDTO;
import com.mf.diceroller.security.user.dto.UserDTO;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
public class GameRoomDTO {
    private String roomId;
    private List<Long> dices;
    private List<UserDTO> players;
    private GameRulesDTO rules;
    private UserDTO gameMaster;
    private String roomName;
    private GameLogDTO gameLog;
    private GameRoomConfigDTO config;
}
