package com.mf.diceroller.room.model.domain;

import com.mf.diceroller.room.model.dto.GameRoomDTO;
import com.mf.diceroller.room.model.dto.GameRoomUserCredentialsDTO;
import com.mf.diceroller.round.model.domain.DiceRollResults;
import com.mf.diceroller.round.model.domain.GameLog;
import com.mf.diceroller.round.model.domain.GameLogEntry;
import com.mf.diceroller.round.model.domain.GameRules;
import com.mf.diceroller.security.user.domain.User;
import com.mf.diceroller.security.user.dto.UserDTO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.time.Instant;
import java.util.List;
import java.util.stream.Collectors;

@Builder
@AllArgsConstructor
@Data
public class GameRoom {
    private String roomId;
    private List<Long> dices;
    private List<User> players;
    private User gameMaster;
    private GameRules rules;
    private GameRoomCredentials credentials;
    private GameLog gameLog;
    private GameRoomConfig config;

    public void closeRoom() {
        this.config.setAllowRoomJoin(false);
    }

    public void openRoom() {
        this.config.setAllowRoomJoin(true);
    }

    public void addRollResults(final User player, final DiceRollResults diceRollResults, final Instant time) {
        gameLog.addEntry(new GameLogEntry(time, diceRollResults, player));
    }

    public GameRoomDTO asDTO() {
        return new GameRoomDTO(
                this.getRoomId(),
                this.getDices(),
                getPlayerDTOS(),
                this.getRules().asDTO(),
                this.gameMaster.asDTO(),
                credentials.getRoomName(),
                gameLog.asDTO(),
                getConfig().asDTO()
        );
    }

    private List<UserDTO> getPlayerDTOS() {
        return this.getPlayers()
                .stream()
                .map(player -> player.asDTO())
                .collect(Collectors.toList());
    }

    public GameRoomUserCredentialsDTO asGameRoomCredentialsMasterDTO() {
        return new GameRoomUserCredentialsDTO(this.getRoomId(), this.getGameMaster().asUserCredentialsDTO());
    }
}
