package com.mf.diceroller.room.model.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class GameRoomConfigDTO {
    private boolean allowRoomJoin;
    private boolean autoEnablePlayers;
    private boolean autoEnableDices;
}
