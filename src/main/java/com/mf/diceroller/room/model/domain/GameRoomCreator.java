package com.mf.diceroller.room.model.domain;

import com.mf.diceroller.room.model.dto.GameRoomSetupDTO;
import com.mf.diceroller.round.model.domain.GameLog;
import com.mf.diceroller.round.model.domain.GameRules;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static java.util.Objects.requireNonNull;

public class GameRoomCreator {
    public static GameRoom from(final GameRoomSetupDTO dto) {
        requireNonNull(dto);

        return GameRoom.builder()
                .roomId(UUID.randomUUID().toString())
                .dices(dto.getDices())
                .credentials(GameRoomCredentialsCreator.from(dto.getRoomCredentials()))
                .players(new ArrayList<>())
                .gameMaster(null)
                .rules(new GameRules(getRulesDices(dto), new ArrayList<>()))
                .gameLog(new GameLog())
                .config(GameRoomConfigCreator.from(dto.getConfig()))
                .build();
    }

    private static List<Long> getRulesDices(final GameRoomSetupDTO dto) {
        return dto.getConfig().isAutoEnableDices()
                ? dto.getDices()
                : new ArrayList<>();
    }

}
