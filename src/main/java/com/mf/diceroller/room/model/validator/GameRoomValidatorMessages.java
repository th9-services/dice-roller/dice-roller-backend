package com.mf.diceroller.room.model.validator;

public enum GameRoomValidatorMessages {
    NO_DICES,
    INVALID_DICES,
    ROOM_NAME_TAKEN,
    NO_CREDENTIALS,
    INVALID_ROOM_NAME,
    INVALID_ROOM_PASSWORD,
    INVALID_NICKNAME,
    INVALID_CONFIG
}
