package com.mf.diceroller.room.model.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "ROOM_NOT_FOUND")
public class RoomNotFoundException extends RuntimeException {
}
