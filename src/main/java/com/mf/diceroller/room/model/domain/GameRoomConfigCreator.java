package com.mf.diceroller.room.model.domain;


import com.mf.diceroller.room.model.dto.GameRoomConfigDTO;

public class GameRoomConfigCreator {
    public static GameRoomConfig from(final GameRoomConfigDTO config) {
        return new GameRoomConfig(
                config.isAllowRoomJoin(),
                config.isAutoEnablePlayers(),
                config.isAutoEnableDices()
        );
    }
}
