package com.mf.diceroller.room.model.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@AllArgsConstructor
@EqualsAndHashCode
@Builder
public class GameRoomCredentials {
    private String roomName;
    private String roomPassword;
}
