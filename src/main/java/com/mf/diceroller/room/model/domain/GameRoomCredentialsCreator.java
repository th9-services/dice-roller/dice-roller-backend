package com.mf.diceroller.room.model.domain;

import com.mf.diceroller.room.model.dto.GameRoomCredentialsDTO;

public class GameRoomCredentialsCreator {
    public static GameRoomCredentials from(final GameRoomCredentialsDTO dto) {
        return GameRoomCredentials.builder()
                .roomName(dto.getRoomName())
                .roomPassword(dto.getRoomPassword())
                .build();
    }
}
