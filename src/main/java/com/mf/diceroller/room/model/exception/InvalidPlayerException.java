package com.mf.diceroller.room.model.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_ACCEPTABLE, reason = "INVALID_PLAYER")
public class InvalidPlayerException extends RuntimeException {
}
