package com.mf.diceroller.room.model.domain;

import com.mf.diceroller.room.model.dto.GameRoomConfigDTO;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class GameRoomConfig {
    private boolean allowRoomJoin;
    private boolean autoEnablePlayers;
    private boolean autoEnableDices;

    public GameRoomConfigDTO asDTO() {
        return new GameRoomConfigDTO(
                allowRoomJoin,
                autoEnablePlayers,
                autoEnableDices
        );
    }
}
