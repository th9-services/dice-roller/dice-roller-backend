package com.mf.diceroller.room.model.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Getter
public class GameRoomJoinDTO {
    private GameRoomCredentialsDTO roomCredentials;
    private String playerNickname;
}
