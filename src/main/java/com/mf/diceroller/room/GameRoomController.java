package com.mf.diceroller.room;

import com.mf.diceroller.room.model.dto.*;
import com.mf.diceroller.room.service.GameRoomCredentialsModelAssembler;
import com.mf.diceroller.room.service.GameRoomModelAssembler;
import com.mf.diceroller.room.service.GameRoomsService;
import com.mf.diceroller.security.user.dto.UserDTO;
import lombok.RequiredArgsConstructor;
import org.springframework.hateoas.EntityModel;
import org.springframework.http.ResponseEntity;
import org.springframework.http.codec.ServerSentEvent;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;

import java.util.List;

@RequestMapping("api/room")
@RestController
@RequiredArgsConstructor
public class GameRoomController {
    private final GameRoomsService gameRoomsService;
    private final GameRoomCredentialsModelAssembler gameRoomCredentialsModelAssembler;
    private final GameRoomModelAssembler gameRoomModelAssembler;


    @GetMapping(path = "/room-info-stream")
    public Flux<ServerSentEvent<GameRoomDTO>> getRoomInfoStream(@RequestParam String roomId) {
        return this.gameRoomsService.getGameRoomStream(roomId)
                .map(room -> ServerSentEvent.<GameRoomDTO>builder()
                        .data(room)
                        .build());
    }

    @PostMapping("/create-room")
    public EntityModel<GameRoomUserCredentialsDTO> createGameRoom(@RequestBody GameRoomSetupDTO gameRoomSetupDTO) {
        final GameRoomUserCredentialsDTO gameRoom = this.gameRoomsService.createRoomAndGM(gameRoomSetupDTO);
        return this.gameRoomCredentialsModelAssembler.toGameMasterModel(gameRoom);
    }

    @PatchMapping("/destroy-room")
    @PreAuthorize("hasAnyAuthority('GAME_MASTER')")
    public ResponseEntity<?> destroyRoom(@RequestParam String roomId) {
        this.gameRoomsService.destroyGameRoom(roomId);
        return ResponseEntity
                .ok()
                .build();
    }

    @PatchMapping("/close-room")
    @PreAuthorize("hasAnyAuthority('GAME_MASTER')")
    public ResponseEntity<?> closeRoom(@RequestParam String roomId) {
        this.gameRoomsService.closeGameRoom(roomId);
        return ResponseEntity
                .ok()
                .build();
    }

    @PatchMapping("/open-room")
    @PreAuthorize("hasAnyAuthority('GAME_MASTER')")
    public ResponseEntity<?> openRoom(@RequestParam String roomId) {
        this.gameRoomsService.openGameRoom(roomId);
        return ResponseEntity
                .ok()
                .build();
    }

    @PatchMapping("/update-dices")
    @PreAuthorize("hasAnyAuthority('GAME_MASTER')")
    public ResponseEntity<?> updateDices(@RequestParam String roomId, @RequestBody List<Long> dices) {
        this.gameRoomsService.updateDices(roomId, dices);
        return ResponseEntity
                .ok()
                .build();
    }

    @PatchMapping("/update-config")
    @PreAuthorize("hasAnyAuthority('GAME_MASTER')")
    public ResponseEntity<?> updateConfig(@RequestParam String roomId, @RequestBody GameRoomConfigDTO config) {
        this.gameRoomsService.updateConfig(roomId, config);
        return ResponseEntity
                .ok()
                .build();
    }

    @PostMapping("/join-room")
    public EntityModel<GameRoomUserCredentialsDTO> joinRoom(@RequestBody GameRoomJoinDTO gameRoomJoinDTO) {
        final GameRoomUserCredentialsDTO gameRoom = this.gameRoomsService.joinRoom(gameRoomJoinDTO);
        return this.gameRoomCredentialsModelAssembler.toModel(gameRoom);
    }

    @PatchMapping("/kick-player")
    @PreAuthorize("hasAnyAuthority('GAME_MASTER')")
    public ResponseEntity<?> kickPlayer(@RequestParam String roomId, @RequestBody UserDTO player) {
        this.gameRoomsService.kickPlayer(roomId, player);
        return ResponseEntity
                .ok()
                .build();
    }
}
