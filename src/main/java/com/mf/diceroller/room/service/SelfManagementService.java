package com.mf.diceroller.room.service;

import com.mf.diceroller.room.model.domain.GameRoomExtended;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.time.Instant;

@Service
@RequiredArgsConstructor
public class SelfManagementService {
    private final GameRoomsService gameRoomsService;
    private final GameRoomCacheService gameRoomCacheService;

    @Value("${self.management.room.idle.milliseconds}")
    private Long roomMaxIdle;

    @Scheduled(fixedRateString = "${self.management.rate.milliseconds}", initialDelayString = "${self.management.rate.milliseconds}")
    public void destroyStaleRooms() {
        gameRoomCacheService.getRooms().stream()
                .filter(this::isStale)
                .map(room -> room.getLastValue().getRoomId())
                .forEach(gameRoomsService::destroyGameRoom);
    }

    private boolean isStale(GameRoomExtended gameRoomExtended) {
        return Instant.now().toEpochMilli() > gameRoomExtended.getLastActivity().getTime() + roomMaxIdle;
    }

}
