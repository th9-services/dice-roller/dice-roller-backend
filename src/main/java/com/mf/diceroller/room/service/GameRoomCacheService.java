package com.mf.diceroller.room.service;

import com.mf.diceroller.room.model.domain.GameRoomCredentials;
import com.mf.diceroller.room.model.domain.GameRoomExtended;
import com.mf.diceroller.room.model.exception.RoomNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

@Service
public class GameRoomCacheService {
    final private Map<String, GameRoomExtended> rooms = new ConcurrentHashMap<>();

    Collection<GameRoomExtended> getRooms() {
        return rooms.values();
    }

    int getRoomsCount() {
        return rooms.size();
    }

    void saveRoom(final GameRoomExtended gameRoomExtended) {
        rooms.put(gameRoomExtended.getLastValue().getRoomId(), gameRoomExtended);
    }

    void destroyRoom(final String roomId) {
        getGameRoomById(roomId).complete();
        rooms.remove(roomId);
    }

    Optional<GameRoomExtended> getRoomByName(final String roomName) {
        return rooms.values()
                .stream()
                .filter(room -> room.getLastValue().getCredentials().getRoomName().equals(roomName))
                .findFirst();
    }

    GameRoomExtended getGameRoomByCredentials(final GameRoomCredentials credentials) throws RoomNotFoundException {
        return this.rooms.values()
                .stream()
                .filter(room -> room.getLastValue().getCredentials().equals(credentials))
                .findFirst()
                .orElseThrow(RoomNotFoundException::new);
    }

    GameRoomExtended getGameRoomById(final String roomId) throws RoomNotFoundException {
        if (!this.isPresent(roomId)) {
            throw new RoomNotFoundException();
        }
        return this.rooms.get(roomId);
    }

    boolean isPresent(String roomId) {
        return this.rooms.containsKey(roomId);
    }
}
