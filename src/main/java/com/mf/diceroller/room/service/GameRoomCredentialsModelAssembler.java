package com.mf.diceroller.room.service;

import com.mf.diceroller.room.GameRoomController;
import com.mf.diceroller.room.model.dto.GameRoomUserCredentialsDTO;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.stereotype.Component;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@Component
public class GameRoomCredentialsModelAssembler implements RepresentationModelAssembler<GameRoomUserCredentialsDTO, EntityModel<GameRoomUserCredentialsDTO>> {

    @Override
    public EntityModel<GameRoomUserCredentialsDTO> toModel(GameRoomUserCredentialsDTO entity) {
        return new EntityModel<>(entity,
                linkTo(methodOn(GameRoomController.class).getRoomInfoStream(entity.getRoomId())).withSelfRel()
        );
    }

    public EntityModel<GameRoomUserCredentialsDTO> toGameMasterModel(GameRoomUserCredentialsDTO entity) {
        return this.toModel(entity)
                .add(
                        linkTo(methodOn(GameRoomController.class).destroyRoom(entity.getRoomId())).withRel("destroy"),
                        linkTo(methodOn(GameRoomController.class).closeRoom(entity.getRoomId())).withRel("close"),
                        linkTo(methodOn(GameRoomController.class).openRoom(entity.getRoomId())).withRel("open")
                );
    }
}
