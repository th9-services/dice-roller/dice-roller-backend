package com.mf.diceroller.room.service;

import com.mf.diceroller.room.model.domain.*;
import com.mf.diceroller.room.model.dto.*;
import com.mf.diceroller.room.model.exception.*;
import com.mf.diceroller.round.model.domain.DiceRollResults;
import com.mf.diceroller.round.model.domain.DiceRollSetupCreator;
import com.mf.diceroller.round.model.domain.GameRulesCreator;
import com.mf.diceroller.round.model.dto.DiceRollSetupDTO;
import com.mf.diceroller.round.model.dto.DiceRollsResultsDTO;
import com.mf.diceroller.round.model.dto.GameRulesDTO;
import com.mf.diceroller.security.user.UserService;
import com.mf.diceroller.security.user.domain.CustomUserDetails;
import com.mf.diceroller.security.user.domain.User;
import com.mf.diceroller.security.user.domain.UserType;
import com.mf.diceroller.security.user.dto.UserDTO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import reactor.core.publisher.DirectProcessor;
import reactor.core.publisher.Flux;
import reactor.core.publisher.FluxProcessor;

import java.time.Instant;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
@RequiredArgsConstructor
public class GameRoomsService {
    private final UserService userService;
    private final GameRoomCacheService gameRoomCacheService;
    private final GameRoomValidator gameRoomValidator;

    @Value("${game.room.max.count}")
    private Long roomLimit;
    @Value("${game.room.players.max.count}")
    private Integer maxPlayers;

    public List<GameRoomDTO> getAllRooms() {
        return gameRoomCacheService.getRooms().stream()
                .map(GameRoomExtended::getLastValue)
                .map(GameRoom::asDTO)
                .collect(Collectors.toList());
    }

    public GameRoomUserCredentialsDTO createRoomAndGM(final GameRoomSetupDTO roomSetup) {
        if (gameRoomCacheService.getRoomsCount() == this.roomLimit) {
            throw new RoomLimitReachedException();
        } else {
            gameRoomValidator.validate(roomSetup);
            return this.createGameRoom(roomSetup)
                    .asGameRoomCredentialsMasterDTO();
        }
    }

    private GameRoom createGameRoom(final GameRoomSetupDTO roomSetup) {
        final FluxProcessor<GameRoom, GameRoom> processor = DirectProcessor.<GameRoom>create().serialize();
        final GameRoom room = GameRoomCreator.from(roomSetup);
        final User gameMaster = this.userService.generateGameMaster(room.getRoomId(), roomSetup.getPlayerNickname());
        room.setGameMaster(gameMaster);

        final GameRoomExtended flux = new GameRoomExtended(processor, processor.sink(), room);
        gameRoomCacheService.saveRoom(flux);
        flux.update();
        return room;
    }

    public void destroyGameRoom(final String roomId) {
        gameRoomCacheService.destroyRoom(roomId);
        this.userService.clearRoom(roomId);
    }

    public Flux<GameRoomDTO> getGameRoomStream(final String roomId) {
        final GameRoomExtended room = gameRoomCacheService.getGameRoomById(roomId);
        return Flux.merge(Flux.just(room.getLastValue()), room.getStream())
                .map(GameRoom::asDTO);
    }

    public void closeGameRoom(final String roomId) {
        gameRoomCacheService.getGameRoomById(roomId)
                .closeRoom();
    }

    public void openGameRoom(final String roomId) {
        gameRoomCacheService.getGameRoomById(roomId)
                .openRoom();
    }

    public void updateRules(final String roomId, final GameRulesDTO rulesDTO) {
        gameRoomValidator.validate(rulesDTO);

        gameRoomCacheService.getGameRoomById(roomId)
                .updateRules(GameRulesCreator.from(rulesDTO, roomId));
    }

    public void updateDices(final String roomId, final List<Long> dices) {
        gameRoomValidator.validate(dices);

        gameRoomCacheService.getGameRoomById(roomId)
                .updateDices(dices);
    }

    public void updateConfig(final String roomId, final GameRoomConfigDTO config) {
        gameRoomValidator.validate(config);

        gameRoomCacheService.getGameRoomById(roomId)
                .updateConfig(GameRoomConfigCreator.from(config));
    }

    public DiceRollsResultsDTO makeARoll(final DiceRollSetupDTO diceRollSetupDTO, final String roomId, final CustomUserDetails userDetails) {
        gameRoomValidator.validate(diceRollSetupDTO);

        final User player = this.userService.createPlayer(userDetails);
        final GameRoomExtended gameRoomExtended = gameRoomCacheService.getGameRoomById(roomId);
        validatePlayerRollRequest(roomId, player);

        final DiceRollResults diceRollResults = player.rollDices(DiceRollSetupCreator.from(diceRollSetupDTO));

        gameRoomExtended.addRollResults(player, diceRollResults, Instant.now());
        return diceRollResults.asDTO();
    }

    private void validatePlayerRollRequest(final String roomId, final User player) {
        if (!player.isInRoom(roomId)) {
            throw new RoomNoAccessException();
        }
    }

    public GameRoomUserCredentialsDTO joinRoom(final GameRoomJoinDTO gameRoomJoinDTO) {
        gameRoomValidator.validate(gameRoomJoinDTO);

        final GameRoomCredentials credentials = GameRoomCredentialsCreator.from(gameRoomJoinDTO.getRoomCredentials());
        final GameRoomExtended gameRoomExtended = gameRoomCacheService.getGameRoomByCredentials(credentials);

        if (!gameRoomExtended.getLastValue().getConfig().isAllowRoomJoin()) {
            throw new RoomNotOpenException();
        }

        if (gameRoomExtended.getLastValue().getPlayers().size() >= maxPlayers) {
            throw new PlayersLimitReachedException();
        }

        final User player = this.userService.generatePlayer(gameRoomExtended.getLastValue().getRoomId(), gameRoomJoinDTO.getPlayerNickname());

        gameRoomExtended.addPlayer(player);

        return new GameRoomUserCredentialsDTO(gameRoomExtended.getLastValue().getRoomId(), player.asUserCredentialsDTO());
    }

    public void kickPlayer(final String roomId, final UserDTO player) {
        final GameRoomExtended gameRoomExtended = gameRoomCacheService.getGameRoomById(roomId);
        User user = gameRoomExtended.getPlayerByUsername(player.getUsername())
                .orElseThrow(InvalidPlayerException::new);

        if (user.getUserType().equals(UserType.GAME_MASTER)) {
            throw new InvalidPlayerException();
        }
        userService.deletePlayer(user);
        gameRoomExtended.kickPlayer(user);

    }

    public boolean roomExist(String roomId) {
        return gameRoomCacheService.isPresent(roomId);
    }
}
