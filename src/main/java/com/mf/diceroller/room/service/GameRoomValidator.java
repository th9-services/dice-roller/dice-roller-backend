package com.mf.diceroller.room.service;

import com.mf.diceroller.exception.InvalidDataError;
import com.mf.diceroller.exception.InvalidDataException;
import com.mf.diceroller.room.model.dto.GameRoomConfigDTO;
import com.mf.diceroller.room.model.dto.GameRoomCredentialsDTO;
import com.mf.diceroller.room.model.dto.GameRoomJoinDTO;
import com.mf.diceroller.room.model.dto.GameRoomSetupDTO;
import com.mf.diceroller.room.model.validator.GameRoomValidatorMessages;
import com.mf.diceroller.round.model.dto.DiceRollSetupDTO;
import com.mf.diceroller.round.model.dto.GameRulesDTO;
import com.mf.diceroller.security.user.dto.UserDTO;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;


@Service
@RequiredArgsConstructor
public class GameRoomValidator {
    private static final int TEXT_MIN_LENGTH = 4;
    private static final int TEXT_MAX_LENGTH = 100;
    private final GameRoomCacheService gameRoomCacheService;

    void validate(final GameRoomJoinDTO join) {
        List<InvalidDataError> errors = Stream.of(
                validateRoomCredentials(join.getRoomCredentials()),
                validatePlayerNickname(join.getPlayerNickname())
        )
                .flatMap(List::stream)
                .collect(Collectors.toList());

        if (!errors.isEmpty()) {
            throw new InvalidDataException(errors);
        }
    }

    void validate(final GameRoomSetupDTO roomSetup) {
        List<InvalidDataError> errors = Stream.of(
                validateDicesRequired(roomSetup.getDices()),
                validateRoomCredentialsUnique(roomSetup.getRoomCredentials()),
                validateGameRoomConfig(roomSetup.getConfig()),
                validatePlayerNickname(roomSetup.getPlayerNickname())
        )
                .flatMap(List::stream)
                .collect(Collectors.toList());

        if (!errors.isEmpty()) {
            throw new InvalidDataException(errors);
        }
    }

    void validate(final List<Long> dices) {
        List<InvalidDataError> errors = validateDicesRequired(dices);

        if (!errors.isEmpty()) {
            throw new InvalidDataException(errors);
        }
    }

    void validate(final GameRulesDTO rulesDTO) {
        List<InvalidDataError> errors = Stream.of(
                validateDices(rulesDTO.getDices()),
                validatePlayers(rulesDTO.getPlayers())
        )
                .flatMap(List::stream)
                .collect(Collectors.toList());

        if (!errors.isEmpty()) {
            throw new InvalidDataException(errors);
        }
    }

    void validate(final DiceRollSetupDTO diceRollSetupDTO) {
        List<InvalidDataError> errors = validateDicesRequired(diceRollSetupDTO.getDices());

        if (!errors.isEmpty()) {
            throw new InvalidDataException(errors);
        }
    }

    void validate(final GameRoomConfigDTO configDTO) {
        List<InvalidDataError> errors = validateGameRoomConfig(configDTO);

        if (!errors.isEmpty()) {
            throw new InvalidDataException(errors);
        }
    }

    private List<InvalidDataError> validateDicesRequired(final List<Long> dices) {
        if (dices == null || dices.isEmpty()) {
            final List<InvalidDataError> errors = new ArrayList<>();
            errors.add(new InvalidDataError("dices", GameRoomValidatorMessages.NO_DICES.toString()));
            return errors;
        }

        return validateDices(dices);
    }

    private List<InvalidDataError> validateDices(final List<Long> dices) {
        final List<InvalidDataError> errors = new ArrayList<>();

        if (dices != null) {
            dices.stream()
                    .filter(this::isDiceInvalid)
                    .findFirst()
                    .ifPresent(dice -> errors.add(new InvalidDataError("dices", GameRoomValidatorMessages.INVALID_DICES.toString())));
        }

        return errors;
    }

    private boolean isDiceInvalid(Long dice) {
        return dice <= 1;
    }

    private List<InvalidDataError> validatePlayers(List<UserDTO> players) {
        final List<InvalidDataError> errors = new ArrayList<>();

        if (players != null) {
            players.stream()
                    .map(UserDTO::getNickname)
                    .filter(this::isTextInvalid)
                    .findFirst()
                    .ifPresent(nickname -> errors.add(new InvalidDataError("players", GameRoomValidatorMessages.INVALID_NICKNAME.toString())));
        }

        return errors;
    }

    private List<InvalidDataError> validateRoomCredentialsUnique(final GameRoomCredentialsDTO roomCredentials) {
        final List<InvalidDataError> errors = validateRoomCredentials(roomCredentials);

        if (roomCredentials != null) {
            gameRoomCacheService.getRoomByName(roomCredentials.getRoomName())
                    .ifPresent(r -> errors.add(new InvalidDataError("roomCredentials", GameRoomValidatorMessages.ROOM_NAME_TAKEN.toString())));
        }

        return errors;
    }

    private List<InvalidDataError> validateRoomCredentials(final GameRoomCredentialsDTO roomCredentials) {
        if (roomCredentials == null) {
            return Collections.singletonList(new InvalidDataError("roomCredentials", GameRoomValidatorMessages.NO_CREDENTIALS.toString()));
        }

        final List<InvalidDataError> errors = new ArrayList<>();

        if (isTextInvalid(roomCredentials.getRoomName())) {
            errors.add(new InvalidDataError("roomCredentials", GameRoomValidatorMessages.INVALID_ROOM_NAME.toString()));
        }
        if (isTextInvalid(roomCredentials.getRoomPassword())) {
            errors.add(new InvalidDataError("roomCredentials", GameRoomValidatorMessages.INVALID_ROOM_PASSWORD.toString()));
        }
        return errors;
    }

    private List<InvalidDataError> validateGameRoomConfig(final GameRoomConfigDTO config) {
        if (config == null) {
            return Collections.singletonList(new InvalidDataError("config", GameRoomValidatorMessages.INVALID_CONFIG.toString()));
        }

        return new ArrayList<>();
    }

    private List<InvalidDataError> validatePlayerNickname(final String playerNickname) {
        final List<InvalidDataError> errors = new ArrayList<>();
        if (isTextInvalid(playerNickname)) {
            errors.add(new InvalidDataError("playerNickname", GameRoomValidatorMessages.INVALID_NICKNAME.toString()));
        }

        return errors;
    }

    private boolean isTextInvalid(final String text) {
        return StringUtils.isEmpty(text)
                || text.length() < TEXT_MIN_LENGTH
                || text.length() > TEXT_MAX_LENGTH;
    }

}
