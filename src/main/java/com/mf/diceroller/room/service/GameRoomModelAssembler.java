package com.mf.diceroller.room.service;

import com.mf.diceroller.room.GameRoomController;
import com.mf.diceroller.room.model.dto.GameRoomDTO;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.stereotype.Component;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@Component
public class GameRoomModelAssembler implements RepresentationModelAssembler<GameRoomDTO, EntityModel<GameRoomDTO>> {
    @Override
    public EntityModel<GameRoomDTO> toModel(GameRoomDTO entity) {
        return new EntityModel<>(entity,
                linkTo(methodOn(GameRoomController.class).getRoomInfoStream(entity.getRoomId())).withSelfRel(),
                linkTo(methodOn(GameRoomController.class).destroyRoom(entity.getRoomId())).withRel("destroy")
        );
    }
}
