package com.mf.diceroller.exception;

import lombok.Value;

@Value
public class InvalidDataError {
    String field;
    String reason;
}
