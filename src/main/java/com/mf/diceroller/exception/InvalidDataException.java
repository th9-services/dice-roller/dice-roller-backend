package com.mf.diceroller.exception;

import lombok.Getter;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.List;

@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "INVALID_DATA")
public class InvalidDataException extends RuntimeException {
    @Getter
    private List<InvalidDataError> errors;

    public InvalidDataException(List<InvalidDataError> errors) {
        this.errors = errors;
    }
}
