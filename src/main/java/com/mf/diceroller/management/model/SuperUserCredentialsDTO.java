package com.mf.diceroller.management.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Getter
public class SuperUserCredentialsDTO {
private String username;
private String password;
}
