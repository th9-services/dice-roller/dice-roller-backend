package com.mf.diceroller.management;

import com.mf.diceroller.management.model.SuperUserCredentialsDTO;
import com.mf.diceroller.room.model.dto.GameRoomDTO;
import com.mf.diceroller.room.service.GameRoomModelAssembler;
import com.mf.diceroller.room.service.GameRoomsService;
import lombok.RequiredArgsConstructor;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("api/management")
@RestController
@RequiredArgsConstructor
public class ManagementController {
    private final GameRoomsService gameRoomsService;
    private final GameRoomModelAssembler gameRoomModelAssembler;
    private final ManagementService managementService;


    @PostMapping("/authenticate")
    @PreAuthorize("hasAnyAuthority('SUPER_USER')")
    public ResponseEntity<?> authenticate(@RequestBody SuperUserCredentialsDTO credentials) {
        return ResponseEntity
                .ok(true);
    }

    @GetMapping(path = "/room/list")
    @PreAuthorize("hasAnyAuthority('SUPER_USER')")
    public CollectionModel<EntityModel<GameRoomDTO>> getRooms() {
        final List<GameRoomDTO> rooms = this.gameRoomsService.getAllRooms();
        return this.gameRoomModelAssembler.toCollectionModel(rooms);
    }

    @PatchMapping("/room/destroy")
    @PreAuthorize("hasAnyAuthority('SUPER_USER')")
    public ResponseEntity<?> destroyRooms(@RequestParam List<String> roomId) {
        managementService.destroyExistingRooms(roomId);
        return ResponseEntity
                .ok()
                .build();
    }
}
