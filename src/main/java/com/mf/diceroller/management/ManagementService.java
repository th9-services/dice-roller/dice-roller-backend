package com.mf.diceroller.management;

import com.mf.diceroller.room.service.GameRoomsService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ManagementService {
    private final GameRoomsService gameRoomsService;

    public void destroyExistingRooms(List<String> rooms) {
        rooms.stream()
                .filter(gameRoomsService::roomExist)
                .forEach(gameRoomsService::destroyGameRoom);
    }
}
